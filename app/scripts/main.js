;
(function () {
  const __API_HOST__ = 'https://umninvest.bcscapital.ru/api/';

  const __FORM_ID__ = 1921;

  var domIsReady = (function (domIsReady) {
    var isBrowserIeOrNot = function () {
      return (!document.attachEvent || typeof document.attachEvent === 'undefined' ? 'not-ie' : 'ie');
    };

    domIsReady = function (callback) {
      if (callback && typeof callback === 'function') {
        if (isBrowserIeOrNot() !== 'ie') {
          document.addEventListener('DOMContentLoaded', function () {
            return callback();
          });
        } else {
          document.attachEvent('onreadystatechange', function () {
            if (document.readyState === 'complete') {
              return callback();
            }
          });
        }
      } else {
        console.error('The callback is not a function!');
      }
    };

    return domIsReady;
  })(domIsReady || {});

  let headerIsOverScrolled = false;

  const handleScroll = () => {
    const nextHeaderIsOverScrolled = window.scrollY >= 20;
    if (headerIsOverScrolled !== nextHeaderIsOverScrolled) {
      const el = document.querySelector('.header');
      el && el.classList[nextHeaderIsOverScrolled ? 'add' : 'remove']('header-fixed');
      headerIsOverScrolled = nextHeaderIsOverScrolled;
    }
  };

  function setPhoneMask() {
    const inputs = document.querySelectorAll('input[name="phone"]');

    for (var i = 0; i < inputs.length; i++) {
      inputs[i].addEventListener('keydown', function (ev) {
        window.setTimeout(function () {
          var matrix = '+7 (___) ___-__-__',
            i = 0,
            val = ev.target.value.replace(/\D/g, '');

          const def = matrix.replace(/\D/g, '');

          if (def.length >= val.length) val = def;

          ev.target.value = matrix.replace(/./g, function (a) {
            return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? '' : a;
          });
        }, 0);
      });
    }
  }

  function setForms() {
    function setFormLoading(flag) {
      if (flag) {
        document.querySelector('.product_request').classList.add('product_request-loading');
      } else {
        document.querySelector('.product_request').classList.remove('product_request-loading');
      }
    }

    function setFormComplete(form, flag) {
      var completeEl = document.querySelector('.product_request__complete');

      if (flag) {
        hideRequest();
        completeEl.classList.add('product_request__complete-show');
      } else {
        completeEl.classList.remove('product_request__complete-show');
        form.querySelector('input[name="full_name"]').value = '';
        form.querySelector('input[name="phone"]').value = '';
        form.querySelector('input[name="city"]').value = '';
      }
    }

    function sendRequest(form, full_name, phone, city) {
      setFormLoading(true);

      const xhr = new XMLHttpRequest;
      xhr.open('post', __API_HOST__, true);
      xhr.setRequestHeader('Content-Type', 'application/json');

      xhr.onload = xhr.onerror = function () {
        if (!this.response) {
          setFormLoading(false);
          return;
        }

        const res = JSON.parse(this.response);

        if (res.success) {
          setFormComplete(form, true);
          setFormLoading(false);
        } else {
          setFormLoading(false);
        }
      };

      var params = {};
      var search = window.location.search.substring(1);

      if (search) {
        var searchParams = search.split('&');
        for (var i = 0; i < searchParams.length; i++) {
          var pair = searchParams[i].split('=');
          if (pair[0]) {
            params[pair[0]] = pair[1];
          }
        }
      }

      xhr.send(JSON.stringify({
        form_id: __FORM_ID__,
        full_name: full_name,
        phone: phone,
        city: city,
        utmz: params.utmz || null,
        refid: params.refid || null
      }));
    }

    function applyForm(form) {
      const nameField = form.querySelector('input[name="full_name"]');
      const phoneField = form.querySelector('input[name="phone"]');
      const cityField = form.querySelector('input[name="city"]');

      function clearError(field) {
        field.parentNode.classList.remove('request__field-err');
        const errEl = field.parentNode.querySelector('.request__field_err');

        if (errEl) {
          errEl.remove();
        }
      }

      function showError(field) {
        const errEl = document.createElement('span');
        errEl.classList.add('request__field_err');
        errEl.innerHTML = 'Обязательное поле';
        field.parentNode.classList.add('request__field-err');
        field.parentNode.appendChild(errEl);
      }

      function onSubmit(ev) {
        ev.preventDefault();

        clearError(nameField);
        clearError(phoneField);
        clearError(cityField);

        var valid = true;

        if (!/^.+$/.test(nameField.value)) {
          showError(nameField);
          valid = false;
        }

        if (
          phoneField.value.length !== 18 ||
          !/^.+$/.test(phoneField.value)
        ) {
          showError(phoneField);
          valid = false;
        }

        if (!/^.+$/.test(cityField.value)) {
          showError(cityField);
          valid = false;
        }

        if (valid) {
          sendRequest(
            form,
            nameField.value,
            phoneField.value,
            cityField.value
          );
        }
      }

      document.querySelector('.product_request__complete_close').addEventListener('click', function () {
        setFormComplete(form, false);
      });

      document.querySelector('.product_request__complete_paragraph-reset').addEventListener('click', function () {
        setFormComplete(form, false);
      });

      nameField.addEventListener('keyup', function () {
        clearError(nameField)
      });

      phoneField.addEventListener('keyup', function () {
        clearError(phoneField)
      });

      cityField.addEventListener('keyup', function () {
        clearError(cityField)
      });

      form.addEventListener('submit', onSubmit);
    }

    applyForm(document.querySelector('.request_form'));

    let isRequestOpen = false;
    const requestEl = document.querySelector('.product_request');
    const buttons = document.querySelectorAll('.request_button');

    for (var i = 0; i < buttons.length; i++) {
      buttons[i].addEventListener('click', showRequest);
    }

    const requestCloseEl = document.querySelector('.product_request__close');
    requestCloseEl.addEventListener('click', hideRequest);

    function showRequest() {
      requestEl.classList.add('product_request-open');
      window.addEventListener('keyup', handleEsq);
      isRequestOpen = true;
      document.body.classList.add('clipped');
    }

    function hideRequest() {
      requestEl.classList.remove('product_request-open');
      setFormComplete(requestEl.querySelector('form'), false);
      window.removeEventListener('keyup', handleEsq);
      isRequestOpen = false;
      document.body.classList.remove('clipped');
    }

    function handleEsq(ev) {
      if (ev.keyCode === 27) {
        hideRequest();
      }
    }
  }

  function setSlider() {
    const sliderElement = document.querySelector('.about_product__inner');
    const slideElements = sliderElement.querySelectorAll('.about_product__card');

    const count = slideElements.length;

    const sliderIndicatorsWrap = document.createElement('div');
    sliderIndicatorsWrap.classList.add('about_product__indicators');

    let html = '';

    for (let i = 0; i < count; i++) {
      html += '<div></div>';
    }

    sliderIndicatorsWrap.innerHTML = html;
    sliderElement.parentNode.append(sliderIndicatorsWrap);

    const sliderIndicators = sliderIndicatorsWrap.querySelectorAll('div');
    sliderIndicators[0].classList.add('current');

    const {
      width: sliderWidthOuter
    } = sliderElement.getBoundingClientRect();

    const sliderWidth = sliderWidthOuter;

    const {
      width: slideWidthInner
    } = slideElements[0].getBoundingClientRect();

    const slideWidth = slideWidthInner + 15;

    let startPosX = 0;
    let startPosY = 0;
    let left = 0;
    let maxPath = sliderWidth - window.innerWidth;

    function setLeft(value) {
      left = value;

      if (value > 0) left = 0;
      if (value < -maxPath) left = -maxPath;

      sliderElement.style.left = `${left}px`;
    }

    function moveDrag(ev) {
      const currentPosX = ev.changedTouches[0].clientX;
      const currentPosY = ev.changedTouches[0].clientY;

      const pathX = currentPosX - startPosX;
      const pathY = currentPosY - startPosY;

      if (Math.abs(pathX) > Math.abs(pathY)) {
        setLeft(pathX);
      }
    }

    function stopDrag(ev) {
      let currentPosX = ev.changedTouches[0].clientX;
      const index = Math.round((currentPosX - startPosX) / slideWidth);

      sliderElement.classList.remove('move');
      setLeft(index * slideWidth);

      const indicatorIndex = Math.max(0, Math.min(-index, sliderIndicators.length - 1));

      sliderIndicatorsWrap.querySelector('.current').classList.remove('current');
      sliderIndicators[indicatorIndex].classList.add('current');

      sliderElement.removeEventListener('touchmove', moveDrag);
      sliderElement.removeEventListener('touchend', stopDrag);
    }

    function startDrag(ev) {
      sliderElement.classList.add('move');
      startPosX = ev.changedTouches[0].clientX - left;
      startPosY = ev.changedTouches[0].clientY;

      sliderElement.addEventListener('touchmove', moveDrag);
      sliderElement.addEventListener('touchend', stopDrag);
    }

    sliderElement.addEventListener('touchstart', startDrag);
  }

  (function (document, window, domIsReady, undefined) {
    domIsReady(function () {
      window.addEventListener('scroll', handleScroll);

      setPhoneMask();
      setForms();
      if (window.innerWidth < 480) {
        setSlider();
      }
    });
  })(document, window, domIsReady);
})();
